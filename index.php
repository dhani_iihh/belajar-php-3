<DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Berlatih PHP</title>
    </head>
    <body>
        <?php 
            class Mobil 
            {
                public $roda = 4;
                private $liter_bensin = 10;
                protected $mesin_mobil = "diesel";
                public function jalan()
                {
                    echo "Mobil berjalan";
                }
                public function jumlah_roda() 
                {
                    echo $this->roda;
                }
                private function isi_bensin()
                {
                    echo "Diisi Pertamax yah kak!";
                }
            }

            class Motor
            {
                protected $roda_motor = 2;
            }

            class MotorSport extends Motor
            {
                protected $maxSpeed;
            }

            $ducatti = new MotorSport;
            echo $ducatti->roda_motor ; // 4 

            $mini = new Mobil();
            $mini->jalan(); // menampilkan echo 'Mobil berjalan'
            echo "<br>";
            echo "<br>";
            echo $mini->roda; // 4
            echo "<br>";
            echo "<br>";
            $mini->jumlah_roda();
            echo "<br>";
            echo "<br>";

            $avanza = new Mobil();
            echo $avanza->isi_bensin();
            echo PHP_EOL;
            echo "<br>";
            echo "<br>";
            echo $avanza->liter_bensin;
            echo PHP_EOL;
            echo "<br>";
            echo "<br>"; 
        ?>
    </body>
</html>